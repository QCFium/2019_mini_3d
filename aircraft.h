#pragma once
#include <DxLib.h>
#include <assert.h>
#include <list>
#include <random>
#include "common.h"

struct Bullet;

#define EXPLOSION_SPHERE_NUM 15
struct Explosion { // class for explosion animation(just drawing a lot of spheres around it)
	VECTOR pos[EXPLOSION_SPHERE_NUM]; // relation pos
	unsigned int green_element[EXPLOSION_SPHERE_NUM];
	int range;
	int r;
	Explosion(int range, int initial_r) {
		this->range = range;
		this->r = initial_r;
		for (int i = 0; i < EXPLOSION_SPHERE_NUM; i++) {
			green_element[i] = std::uniform_int_distribution<>(0, 255)(rnd);
			pos[i] = VGet(
				std::uniform_int_distribution<>(-range, range)(rnd),
				std::uniform_int_distribution<>(-range, range)(rnd),
				std::uniform_int_distribution<>(-range, range)(rnd)
			);
		}
	}
	void draw(VECTOR center_pos) {
		// first, update positions and colors
		for (int i = 0; i < EXPLOSION_SPHERE_NUM; i++) {
			green_element[i] += std::uniform_int_distribution<>(-1, 1)(rnd);
			if (green_element[i] < 0) green_element[i] = 0;
			if (green_element[i] >= 256) green_element[i] = 255;

			/*
			pos[i].x += std::uniform_int_distribution<>(-1, 1)(rnd);
			pos[i].y += std::uniform_int_distribution<>(-1, 1)(rnd);
			pos[i].z += std::uniform_int_distribution<>(-1, 1)(rnd);
			if (pos[i].x < -range) pos[i].x = -range;
			if (pos[i].x > range) pos[i].x = range;
			if (pos[i].y < -range) pos[i].y = -range;
			if (pos[i].y > range) pos[i].y = range;
			if (pos[i].z < -range) pos[i].z = -range;
			if (pos[i].z > range) pos[i].z = range;*/
		}
		for (int i = 0; i < EXPLOSION_SPHERE_NUM; i++) {
			unsigned int color = GetColor(255, green_element[i], 0);
			DrawSphere3D(center_pos + pos[i], r, 32, color, color, TRUE);
		}
		r += 2;
	}
};


#define ENEMY_RANGE 4000 // how far from me can enemy appear/stay alive
#define ENEMY_RANGE_HEIGHT 1000
#define ENEMY_SHOT_RANGE 2000

#define PITCH_LIMIT_UP (DX_PI / 6) // 30 degrees nose up
#define PITCH_LIMIT_DOWN -(DX_PI / 6) // 30 degrees nose down

#define PITCH_LIMIT_ENEMY_UP 0 // (DX_PI / 6) // 30 degrees nose up
#define PITCH_LIMIT_ENEMY_DOWN -(DX_PI / 6) // 30 degrees nose down

#define SPEED_ME 10

struct Aircraft {
	static int enemy_aircraft_handle;

	int model;
	VECTOR pos;
	double speed;
	double roll; // acceleration rate of yaw
	double yaw; // positive : right, negative : left
	double pitch; // positive : up, negativ : down
	bool is_enemy;
	int dead_cnt = -1; // how many frames since the death(enemy only)
	Explosion explosion = Explosion(80, 70);
	int rolling = 0; // positive if rolling right, negative if rolling left, frames remaining, only for enemies
	int rotating = 0; // positive if up, negative if down, frames remaining, only for enemies

	int frames_from_last_shot = 10000; // just a shot limitter

	Aircraft (void);
	Aircraft (const char *model_path);
	Aircraft (int model_handle, int x, int y, int z);
	~Aircraft (void);

	static Aircraft new_at_random(const Aircraft &me); 

	VECTOR getHeading() const;
	double distance(const Aircraft &other) const;
	bool draw();
	void move(std::list<Bullet> &bullets, const Aircraft *me);
	void control(const char keys[]);
	bool collide(const VECTOR &r0);
	std::list<Bullet>::const_iterator check_collide(const std::list<Bullet> &bullets);
	bool disappearing() const;

	void start_roll(int to_left) { rolling = to_left ? -120 : 120; } // for enemies
	void start_rotate(int up) { rotating = up ? 60 : -60; } // for enemies 
};

