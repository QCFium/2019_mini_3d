#include "ui.h"

#define AIM_CIRCLE_R 70
#define AIM_CIRCLE_X (SCREEN_WIDTH / 2)
#define AIM_CIRCLE_Y (SCREEN_HEIGHT / 2 - 2 * AIM_CIRCLE_R)
#define AIM_DISTANCE 2000
#define AIM_LOCK_R 30

static void drawTerrainBox(int height, int x1, int z1, int x2, int z2, unsigned int color) {
	// triangle * 2 => rectangle :)
	DrawTriangle3D(VGet(x1, height, z1), VGet(x1, height, z2), VGet(x2, height, z2), color, TRUE);
	DrawTriangle3D(VGet(x1, height, z1), VGet(x2, height, z1), VGet(x2, height, z2), color, TRUE);
}

#define CHECKER_SIZE 50
#define CHECKER_NUM 50 // caution not to set too big or it'll be extremely slow
/*
static const unsigned int CHECKER_COLORS [] = {
GetColor(150, 0, 150),
GetColor(150, 40, 40)
};*/
static const unsigned int CHECKER_COLORS[] = {
	GetColor(70, 150, 70),
	GetColor(50, 200, 50)
};
void drawTerrain(VECTOR current_pos) {
	float x = current_pos.x;
	float z = current_pos.z;
	for (int i = x / CHECKER_SIZE - CHECKER_NUM; i < x / CHECKER_SIZE + CHECKER_NUM; i++) {
		for (int j = z / CHECKER_SIZE - CHECKER_NUM; j < z / CHECKER_SIZE + CHECKER_NUM; j++) {
			drawTerrainBox(0, CHECKER_SIZE * i, CHECKER_SIZE * j,
				CHECKER_SIZE * (i + 1), CHECKER_SIZE * (j + 1), CHECKER_COLORS[(i + j) & 1]);
		}
	}
}


void drawAimLock(Aircraft &me, std::list<Aircraft> &enemies, std::list<Aircraft>::iterator &target) {
	target = enemies.end();
	for (auto itr = enemies.begin(); itr != enemies.end(); itr++) {
		if (itr->distance(me) > AIM_DISTANCE) continue;
		VECTOR pos_in_screen = ConvWorldPosToScreenPos(itr->pos);
		if (pos_in_screen.z <= 0.0 || pos_in_screen.z >= 1.0) continue; // not in screen
		if (SQUARE(pos_in_screen.x - AIM_CIRCLE_X) + SQUARE(pos_in_screen.y - AIM_CIRCLE_Y) >= SQUARE(AIM_CIRCLE_R)) continue;
		target = itr;
	}
	if (target != enemies.end()) {
		VECTOR pos_in_screen = ConvWorldPosToScreenPos(target->pos);
		assert(pos_in_screen.z > 0.0 && pos_in_screen.z < 1.0);
		DrawCircle(pos_in_screen.x, pos_in_screen.y, AIM_LOCK_R, GetColor(255, 0, 0), FALSE);
		DrawLine(pos_in_screen.x, pos_in_screen.y - AIM_LOCK_R, pos_in_screen.x, pos_in_screen.y + AIM_LOCK_R, GetColor(255, 0, 0));
		DrawLine(pos_in_screen.x - AIM_LOCK_R, pos_in_screen.y, pos_in_screen.x + AIM_LOCK_R, pos_in_screen.y, GetColor(255, 0, 0));
	}
}
void drawFinder() {
	DrawCircle(AIM_CIRCLE_X, AIM_CIRCLE_Y, AIM_CIRCLE_R, GetColor(0, 255, 0), FALSE, 2);
}



