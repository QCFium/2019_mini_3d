#pragma once
#include <DxLib.h>
#include "common.h"

#define BULLET_R 2
struct Bullet {
	bool is_mine;
	VECTOR pos;
	VECTOR speed;
	VECTOR *target = nullptr; // target position
	Bullet(bool is_mine, const VECTOR &pos, const VECTOR &speed) : is_mine(is_mine), pos(pos), speed(speed) {}
	void draw() {
		unsigned int color = is_mine ? GetColor(0, 0, 0) : GetColor(255, 0, 0);
		DrawSphere3D(pos, BULLET_R, 8, color, color, TRUE);
	}
	void move() {
		if (target) {
			if (VDot(speed, (*target - pos)) < 0) target = nullptr; // leaving from the target, cancel target
			else speed += (*target - pos) * 0.002;
		}
		pos += speed;
	}
};


