#pragma once
#include <DxLib.h>
#include "common.h"
#include "aircraft.h"

void drawTerrain(VECTOR current_pos);
void drawAimLock(Aircraft &me, std::list<Aircraft> &enemies, std::list<Aircraft>::iterator &target);
void drawFinder(void);
