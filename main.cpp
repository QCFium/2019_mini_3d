#include <DxLib.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include <math.h>
#include <list>
#include <random>
#include "common.h"
#include "aircraft.h"
#include "bullet.h"
#include "ui.h"

/*
	3D models from :

	https://sites.google.com/site/tobashomesouko/home/hikouki-3dcgdeta

	Thanks !!
*/

#define ENEMIES_NUM_MAX 100000 // unlimited

#define MAX_BULLET_DISTANCE 5000 // erase bullets that is far for more than this

int font_big;
int font_middle;
const char *init() {
	srand(time(NULL));
	if (SetOutApplicationLogValidFlag(FALSE) == -1) return "SetOutApplicationLogValidFlag() failed";
	if (ChangeWindowMode(TRUE) != DX_CHANGESCREEN_OK) return "ChangeWindowMode() failed";
	if (SetGraphMode(SCREEN_WIDTH, SCREEN_HEIGHT, 32) != DX_CHANGESCREEN_OK) return "SetGraphMode() failed";
	if (SetMainWindowText("Cheated")) return "SetMainWindowText() failed";
	if (DxLib_Init() == -1) return "DxLib_Init() failed";
	if (SetDrawScreen(DX_SCREEN_BACK) == -1) return "SetDrawScreen() failed";
	if (SetCameraNearFar(0.1f, 10000) == -1) return "SetCameraNearFar() failed";
	if (SetUseZBuffer3D(TRUE) == -1) return "SetUseZBuffer3D() failed";
	if (SetWriteZBuffer3D(TRUE) == -1) return "SetWriteZBuffer3D() failed";
	font_big = CreateFontToHandle(NULL, 40, 2);
	font_middle = CreateFontToHandle(NULL, 30, 2);
	return NULL;
}

bool description() {
	char keys[256];
	while (!ProcessMessage()) {
		ClearDrawScreen();
		GetHitKeyStateAll(keys);
		if (keys[KEY_INPUT_ESCAPE]) return false;

		// background
		DrawBox(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, GetColor(144, 215, 236), TRUE);

		DrawStringToHandle(300, SCREEN_HEIGHT / 2 - 160,
			"戦闘機を飛ばすゲームです\nキーボードの方向キーで操縦できます\n"
			"スペースキーを押すと、弾が発射できます", GetColor(255, 150, 30), font_big);
		DrawFormatStringToHandle((SCREEN_WIDTH - 500) / 2, SCREEN_HEIGHT / 2, GetColor(50, 50, 100), font_big,
			"Enterキーを押してスタート");
		if (keys[KEY_INPUT_RETURN]) return true;
		ScreenFlip();
	}
	return false;
}

#define ONE_GAME_SECONDS 180
#define RESULT_SECONDS 3
int frame_sum = 0;

// returns if continue gaming
bool game() {
	if (frame_sum >= ONE_GAME_SECONDS * 60) return false;
	Aircraft me("res/plane_me.mqo");
	std::list<Aircraft> enemies;
	std::list<Bullet> bullets;
	int dead_time = -1; // frames from the death(-1 if not dead yet)
	char keys[256] = { 0 };
	int frame_cnt = 0;
	int score = 0;
	int damage = 0;
	while (!ProcessMessage() && !keys[KEY_INPUT_ESCAPE]) {
		ClearDrawScreen();
		int seconds_left = ONE_GAME_SECONDS - frame_sum / 60;
		GetHitKeyStateAll(keys);

		// background
		DrawBox(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, GetColor(144, 215, 236), TRUE);

		frame_sum++;
		if (++frame_cnt % 100 == 0 && enemies.size() < ENEMIES_NUM_MAX)
			enemies.push_back(Aircraft::new_at_random(me)); // add a enemy

		drawTerrain(me.pos);
		if (seconds_left >= 0) {
			// handle enemies
			for (auto itr = enemies.begin(); itr != enemies.end(); ) {
				itr->move(bullets, &me);
				auto collide_bullet = itr->check_collide(bullets);
				if (collide_bullet != bullets.end()) bullets.erase(collide_bullet), score++;
				if ((distance(me.pos, itr->pos) <= ENEMY_RANGE && itr->draw()) || itr->disappearing()) {
					for (auto &j : bullets) if (j.target == &itr->pos) j.target = nullptr; // cancel target to the aircraft
					itr = enemies.erase(itr);
				} else {
					if (frame_cnt % 600 == 0) {
						itr->start_roll(rand() % 2);
						itr->start_rotate(rand() % 2);
					}
					itr++;
				}
			}
			// handle bullets(Z buffer enabled so no warry for sequence of drawing)
			for (auto itr = bullets.begin(); itr != bullets.end(); ) {
				if (distance(itr->pos, me.pos) > MAX_BULLET_DISTANCE) itr = bullets.erase(itr);
				else {
					itr->move();
					SetUseLighting(FALSE);
					itr->draw();
					SetUseLighting(TRUE);
					itr++;
				}
			}
			// collision me
			{
				auto collided = me.check_collide(bullets);
				if (collided != bullets.end()) damage++, bullets.erase(collided);
			}
			// finder, aim lock
			drawFinder();
			std::list<Aircraft>::iterator target;
			drawAimLock(me, enemies, target);
			// draw my aircraft
			if (me.draw() || dead_time != -1) {
				// game over
				dead_time++;
				DrawFormatString(SCREEN_WIDTH / 2 - 50, SCREEN_HEIGHT / 2 - 30, GetColor(255, 0, 0), "Game Over");
				me.pos.y = 5;
				me.pitch = 0;
				me.speed *= 0.98;
				if (dead_time > 300) return true;
			} else {
				me.control(keys);
			}
			me.move(bullets, nullptr);

			static int frames_from_last = 1000;
			if (keys[KEY_INPUT_SPACE] && frames_from_last > 5) {
				frames_from_last = 0;
				bullets.push_back(Bullet(true, me.pos, me.getHeading() * me.speed * 2));
				if (target != enemies.end()) bullets.back().target = &target->pos;
			} else frames_from_last++;
		} else {
			for (auto &bullet : bullets) bullet.draw();
			for (auto &enemy : enemies) enemy.draw();
			me.draw();
			DrawStringToHandle(SCREEN_WIDTH / 2 - 50, SCREEN_HEIGHT / 2 - 20, "終了", GetColor(255, 100, 0), font_big);
			if (frame_sum >= (ONE_GAME_SECONDS + RESULT_SECONDS) * 60) return false;
		}


#		ifdef DEBUG
			int fps = GetFPS();
			unsigned int fps_color = GetColor(255, 255, 255);
			if (fps < 40) fps_color = GetColor(255, 255, 0);
			if (fps < 30) fps_color = GetColor(255, 0, 0);
			DrawFormatString(0, 0, fps_color, "%d FPS", (int) GetFPS());
			DrawFormatString(0, 20, GetColor(255, 255, 255), "Score : %d", score);
			DrawFormatString(0, 40, GetColor(255, 255, 255), "Damage : %d", damage);
#		else
			if (seconds_left < 0) seconds_left = 0;
			DrawFormatStringToHandle(0, 0, GetColor(255, 255, 255), font_big, "残り %01d:%02d", seconds_left / 60, seconds_left % 60);
			DrawFormatStringToHandle(0, 40, GetColor(255, 255, 255), font_middle, "スコア : %d", score);
			DrawFormatStringToHandle(0, 70, GetColor(255, 255, 255), font_middle, "ダメージ : %d", damage);
#		endif

		ScreenFlip();
	}
	return false;
}

int __stdcall WinMain(HINSTANCE _instance, HINSTANCE _pinstance, LPSTR _cmd_args, int _cmd_show) {
	const char *err;
	if ((err = init())) {
		char buf[100];
		snprintf(buf, 100, "echo %s & pause", err);
		system(buf);
		return 1;
	}
	Aircraft::enemy_aircraft_handle = MV1LoadModel("res/plane_enemy.mqo");
	assert(Aircraft::enemy_aircraft_handle != -1);
	assert(MV1SetUseZBuffer(Aircraft::enemy_aircraft_handle, TRUE) != -1);
	assert(MV1SetWriteZBuffer(Aircraft::enemy_aircraft_handle, TRUE) != -1);

	if (description()) while(game());

	DxLib_End();

	return 0;
}
