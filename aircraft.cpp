#include "aircraft.h"
#include "bullet.h"
#include "common.h"

int Aircraft::enemy_aircraft_handle = -1;

Aircraft::Aircraft() {
	pos.x = 50;
	pos.y = 400;
	pos.z = 100;
	speed = SPEED_ME;
	roll = 0;
	pitch = 0;
	yaw = DX_PI; // model inversed
}

Aircraft::Aircraft(const char *model_path) : Aircraft() { // me
	model = MV1LoadModel(model_path);
	assert(MV1SetUseZBuffer(model, TRUE) != -1);
	assert(MV1SetWriteZBuffer(model, TRUE) != -1);
	assert(model != -1);
	MV1SetScale(model, VGet(0.5, 0.5, 0.5));
	is_enemy = false;
}
Aircraft::Aircraft(int model_handle, int x, int y, int z) : Aircraft() { // enemy
	speed = 3;
	this->model = model_handle;
	assert(model != -1);
	MV1SetScale(model, VGet(0.5, 0.5, 0.5));
	pos.x = x;
	pos.y = y;
	pos.z = z;
	is_enemy = true;
}
Aircraft::~Aircraft() {
	if (!is_enemy) MV1DeleteModel(model);
}

Aircraft Aircraft::new_at_random(const Aircraft &me) {
	Aircraft res(enemy_aircraft_handle,
		std::uniform_int_distribution<>(me.pos.x - ENEMY_RANGE, me.pos.x + ENEMY_RANGE)(rnd),
		std::uniform_int_distribution<>(max(0, me.pos.y - ENEMY_RANGE_HEIGHT), me.pos.y + ENEMY_RANGE_HEIGHT)(rnd),
		std::uniform_int_distribution<>(me.pos.z - ENEMY_RANGE, me.pos.z + ENEMY_RANGE)(rnd));
	res.pitch = std::uniform_real_distribution<>(-0.2, 0.7)(rnd);
	res.yaw = std::uniform_real_distribution<>(-DX_PI, DX_PI)(rnd);
	return res;
}


VECTOR Aircraft::getHeading() const {
	VECTOR res = VGet(0, 0, -1); // model inversed
	res = VTransform(res, MGetRotX(pitch));
	res = VTransform(res, MGetRotY(yaw));
	return res;
}
double Aircraft::distance(const Aircraft &other) const {
	return sqrt(SQUARE(pos.x - other.pos.x) + SQUARE(pos.y - other.pos.y) + SQUARE(pos.z - other.pos.z));
}

bool Aircraft::draw() {
	VECTOR camera_pos = pos + VGet(0, 20, 0) + getHeading() * -80;
	if (!is_enemy) SetCameraPositionAndTarget_UpVecY(camera_pos, pos);

	if (dead_cnt == -1) {
		MV1SetPosition(model, pos);
		// pitch/yaw/roll handling
		MV1SetRotationZYAxis(model, VGet(sin(yaw), -sin(pitch), cos(yaw)), VGet(0, 1, 0), roll * 5);
		MV1DrawModel(model);
	} else {
		SetUseLighting(FALSE);
		explosion.draw(pos);
		SetUseLighting(TRUE);
	}
	/*
	DrawLine3D(pos, pos + VGet(0, 20, 0), GetColor(0, 0, 255));
	DrawLine3D(pos + VGet(0, 0, 40), pos + VGet(0, 0, -40), GetColor(0, 0, 255));
	DrawLine3D(pos + VGet(-20, 0, 0), pos + VGet(20, 0, 0), GetColor(0, 0, 255));
	DrawSphere3D(pos, 5, 16, GetColor(255, 0, 0), GetColor(255, 0, 0), TRUE);*/

	return pos.y < 0; // whether it's dead
}
void Aircraft::move(std::list<Bullet> &bullets, const Aircraft *me) {
	if (is_enemy) assert(me);
	if (rolling < 0) rolling++, roll -= 0.005;
	else if (rolling > 0) rolling--, roll += 0.005;
	if (rotating < 0) rotating++, pitch -= 0.004;
	else if (rotating > 0) rotating--, pitch += 0.004;
	if (is_enemy) {
		if (pitch > PITCH_LIMIT_ENEMY_UP) pitch = PITCH_LIMIT_ENEMY_UP;
		if (pitch < PITCH_LIMIT_ENEMY_DOWN) pitch = PITCH_LIMIT_ENEMY_DOWN;
		if (dead_cnt == -1) {
			// avoid flight into terrain
			if (pos.y < 100 && pitch < 0) pitch += 0.01;
			// shot
			if (::distance(me->pos, pos) <= ENEMY_SHOT_RANGE &&
					VDot(VNorm(me->pos - pos), VNorm(getHeading())) > 0.9 && frames_from_last_shot >= 10)
				bullets.push_back({false, pos, VNorm(getHeading()) * SPEED_ME * 2 + VGetRand(10)}), frames_from_last_shot = 0;
			else frames_from_last_shot++;
		}
	}
	
	yaw += roll * 0.1;
	roll *= 0.95; // balance automatically
	pos += getHeading() * speed;
	if (dead_cnt != -1) dead_cnt++;
}
void Aircraft::control(const char keys[]) {
	int roll = 0;
	if (keys[KEY_INPUT_LEFT]) roll--;
	if (keys[KEY_INPUT_RIGHT]) roll++;
	this->roll += roll * 0.01;
	// roll is automatically limited by move()

	int pitch = 0;
	if (keys[KEY_INPUT_UP]) pitch++;
	if (keys[KEY_INPUT_DOWN]) pitch--;
	this->pitch += pitch * 0.01;

	// pitch limitter
	if (this->pitch > PITCH_LIMIT_UP) this->pitch = PITCH_LIMIT_UP;
	if (this->pitch < PITCH_LIMIT_DOWN) this->pitch = PITCH_LIMIT_DOWN;
}
bool Aircraft::collide(const VECTOR &r0) {
	VECTOR related = r0 - pos;
	related = VTransform(related, MGetRotY(-yaw));
	related = VTransform(related, MGetRotX(-pitch));
	return std::abs(related.z) <= 40 && related.y >= 0 && related.y <= 20 && std::abs(related.x) <= 30;
}
std::list<Bullet>::const_iterator Aircraft::check_collide(const std::list<Bullet> &bullets) {
	if (dead_cnt != -1) return bullets.end(); // no overkill...
	auto collide = bullets.end();
	for (auto itr = bullets.begin(); itr != bullets.end(); itr++) {
		if (itr->is_mine != is_enemy) continue;
		if (this->collide(itr->pos)) collide = itr;
	}
	if (collide != bullets.end() && is_enemy) dead_cnt = 0;
	return collide;
}
bool Aircraft::disappearing() const {
	return dead_cnt > 50;
}
