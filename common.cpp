#include "common.h"

std::random_device rnd_dev;
std::mt19937 rnd(rnd_dev());

double distance(VECTOR a, VECTOR b) {
	return sqrt(SQUARE(a.x - b.x) + SQUARE(a.y - b.y) + SQUARE(a.z - b.z));
}

// we don't want to use freaking C-style functions
VECTOR operator + (const VECTOR &r0, const VECTOR &r1) { return VAdd(r0, r1); }
VECTOR operator - (const VECTOR &r0, const VECTOR &r1) { return VSub(r0, r1); }
VECTOR operator * (const VECTOR &r0, double times) { return VScale(r0, times); }

VECTOR &operator += (VECTOR &r0, const VECTOR &r1) { return r0 = r0 + r1; }
VECTOR &operator -= (VECTOR &r0, const VECTOR &r1) { return r0 = r0 - r1; }
VECTOR &operator *= (VECTOR &r0, double times) { return r0 = r0 * times; }


VECTOR VGetRand(double r) {
	return VNorm(VGet(
		std::uniform_int_distribution<>(-1, 1)(rnd),
		std::uniform_int_distribution<>(-1, 1)(rnd),
		std::uniform_int_distribution<>(-1, 1)(rnd)
	));
}
