#pragma once
#include <random>
#include <DxLib.h>

#define SQUARE(x) ((x) * (x))

#define SCREEN_WIDTH 1280
#define SCREEN_HEIGHT 720

// defined in common.cpp
extern std::random_device rnd_dev;
extern std::mt19937 rnd;

double distance(VECTOR, VECTOR); // distance between 2 points

// we don't want to use freaking C-style functions
VECTOR operator + (const VECTOR &, const VECTOR &);
VECTOR operator - (const VECTOR &, const VECTOR &);
VECTOR operator * (const VECTOR &, double);

VECTOR &operator += (VECTOR &, const VECTOR &);
VECTOR &operator -= (VECTOR &, const VECTOR &);
VECTOR &operator *= (VECTOR &, double);

VECTOR VGetRand(double r);
